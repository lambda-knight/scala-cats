package algebras

import cats.{ Semigroup, Monoid }

object catswork {

  case class Person(firstName: String, lastName: String)

  val persMaker = (x: Person, y: Person) => Person(x.firstName + y.firstName, x.lastName + y.lastName)

  implicit val personSemigroup: Semigroup[Person] = new Semigroup[Person] {
    def combine(x: Person, y: Person): Person = persMaker(x, y)
  }

  implicit val personMonoid: Monoid[Person] = new Monoid[Person] {
    def empty: Person = Person("", "")
    def combine(x: Person, y: Person): Person = persMaker(x, y)
  }
}
