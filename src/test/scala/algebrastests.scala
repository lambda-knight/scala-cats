package algebras

import cats.{ Semigroup, Monoid }
import cats.syntax.semigroup._
import org.scalatest._
import org.scalatest.prop.PropertyChecks
import algebras.catswork._

class AlgebrasTests extends FlatSpec with Matchers with PropertyChecks {

  "Semigroups" should "combine" in { forAll { (a: String, b: String, c: String, d: String) =>
      val p1 = Person(a, b)
      val p2 = Person(c, d)
      val p3 = Semigroup[Person].combine(p1, p2)
      p3.firstName shouldBe (a + c)
  }}

  it should "also use special syntax" in { forAll { (a: String, b: String, c: String, d: String) =>
    val p1 = Person(a, b)
    val p2 = Person(c, d)
    (p1 |+| p2).firstName shouldBe (a + c)
  }}

  "Monoids" should "work the same as semigroup when combined" in {
    forAll { (a: String, b: String, c: String, d: String) =>
      val p1 = Person(a, b)
      val p2 = Person(c, d)
      val p3 = Monoid[Person].combine(p1, p2)
      p3.firstName shouldBe (a + c)
    }
  }

  it should "also fold since it provides an identity" in {
    val p1 = Person("a", "b")
    val p2 = Person("c", "d")
    val p3 = Person("e", "f")
    val result = List(p1, p2, p3).foldLeft(Monoid[Person].empty)(Monoid[Person].combine)
    result.firstName shouldBe ("ace")
  }
}
