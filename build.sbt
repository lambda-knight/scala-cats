name := "catswork"
version := "0.0.1"
scalaVersion := "2.12.6"

scalacOptions ++= Seq(
  "-language:reflectiveCalls",
  "-Ywarn-unused-import",
  "-Ypartial-unification"
)

libraryDependencies ++= Seq(
  "org.typelevel" %% "cats-core"  % "1.2.0",
  "org.typelevel" %% "cats-laws" % "1.2.0" % "test",
  "org.scalatest" %% "scalatest" % "3.0.5" % "test"
)
